SRCREV  = "505467466b5751548cca73a32458c0084a8b38cd"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/synquacer:"

LIC_FILES_CHKSUM = "file://license.md;beginline=5;md5=9db9e3d2fb8d9300a6c3d15101b19731 \
                    file://contrib/cmsis/git/LICENSE.txt;md5=e3fc50a88d0a364313df4b21ef20c29e"

SCP_PLATFORM  = "synquacer"
FW_TARGETS = "scp"
SCP_LOG_LEVEL = "WARN"

COMPATIBLE_MACHINE:synquacer = "synquacer"

ROMRAMFW_FILE = "scp_romramfw_${SCP_BUILD_STR}.bin"

SRC_URI:append = " file://0001-FWU-synquacer-Add-FWU-Multi-Bank-Update-and-BL2-boot.patch"
SRC_URI:append = " file://0002-synquacer-allocate-secure-DRAM-for-BL31.patch"

SRC_URI:remove = "file://0001-smt-Make-status-and-length-volatile-for-mod_smt_memo.patch"

do_deploy:append() {
    cd ${DEPLOYDIR}
    tr "\000" "\377" < /dev/zero | dd of=${ROMRAMFW_FILE} bs=1 count=196608
    dd if=scp_romfw.bin of=${ROMRAMFW_FILE} bs=1 conv=notrunc seek=0
    dd if=scp_ramfw.bin of=${ROMRAMFW_FILE} bs=1 seek=65536
}

PV = "2.10.0"
